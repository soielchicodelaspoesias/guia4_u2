#include <fstream>
#include <iostream>
#include <string>
#include "Grafo.h"
using namespace std;

Grafo::Grafo(){}

ofstream fp;
// funcion paara crear el txt
void Grafo::crear_grafo (Nodo *nodo){
  fp.open ("grafo.txt");
  fp << "digraph G {" << endl;
  fp << "node [style=filled fillcolor=yellow];" << endl;
  // se llama a recorrer para escribir todos los datos
  recorrer(nodo);
  fp << "}" << endl;

  fp.close();
  system("dot -Tpng -ografo.png grafo.txt &");

  system("eog grafo.png &");
}
//recorre en árbol en preorden y agrega datos al archivo.
void Grafo::recorrer(Nodo *p) {
  // se rescriben todos los nodos y a dinde apuntan
  if (p != NULL) {
    if (p->izq != NULL) {
      fp <<  p->info << "->" << p->izq->info << ";" << endl;
    } else {
        string cadena = std::to_string(p->info) + "i";
        fp <<  "\"" << cadena << "\"" << " [shape=point];" << endl;
        fp << p->info << "->" << "\"" << cadena << "\"" << ";" << endl;
    }
    if (p->der != NULL) {
      fp << p->info << "->" << p->der->info << ";" << endl;
    } else {
        string cadena = std::to_string(p->info) + "d";
        fp <<  "\"" << cadena << "\""  << " [shape=point];" << endl;
        fp << p->info << "->" << "\"" << cadena << "\"" << ";" << endl;
    }
    recorrer(p->izq);
    recorrer(p->der);
  }
}
// se recorre el arbol en preorden : raiz, izq, der
void Grafo::preorden(Nodo *p) {
  if (p != NULL) {
    cout << p->info << "-";
    preorden(p->izq);
    preorden(p->der);
  }
}
// se recorre el arbol en inorden: izq,raiz, der

void Grafo::inorden(Nodo *p) {
  if (p != NULL) {
    inorden(p->izq);
    cout << p->info << "-";
    inorden(p->der);
  }
}
// se recorre el arbol en posorden : izq, der, raiz

void Grafo::posorden(Nodo *p) {
  if (p != NULL) {
    posorden(p->izq);
    posorden(p->der);
    cout << p->info << "-";
  }
}

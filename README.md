# guia4_u2
*Arboles binarios de busqueda*

*Pre-requisitos*
C++
Make
Ubuntu

*Instalación*
Instalar Make.
abrir terminal y escriba el siguiente codigo:
sudo apt install make

*Problematica*
Se debe realizar un programa que cree arbol binario que contenga numeros enteros no repetor, despues hay que realizar operaciones con este, como insertar datos eliminar, modificar, imprimir en preorden, inorder, posorden, tambien es necesario crear un txt con el cual se construye un archivo png

*Ejecutando*
Al ejecutar el programa se encontrara con un menu de 6 opciones, la primera sea crear arbol, es esta se le pedira la primera raiz, y si desea crear un nodo izquierdo y uno derescho despues se repetira el proceso con los nodos que valla creando. La segunda opcion es insertar datos en esta se le pedira el nodo al que sedea insertar el numero y el numero a ingresar, si el nodo tiene dos hijos no podra agregar el nuevo numero, al igual si ingrea un numero menos y el lado izquiedo esta ocupado, o si agrega un numero mayor y el lado derecho esta ocupado. La tercera opcion sera eliminar un nodo en esta le edira el nodo a eliminar y se borrara este. La cuarta opcion es modificar un elemento en este se le pedira el nodo a modificar y el nuevo valor, si ingrega un nodo que no  existe retornara al menu. La quinta opcion es mostrar el contedino del arbol en preorden, inorden y posorden. Finalmente la sexta opcion sera salir y se terminara el programa.

*Construido con C++*

*Librerias*
Iostream
list
fstream
string

*Version 0.1*

*Autor*
Luis Rebolledo


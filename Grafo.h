#include <fstream>
#include <iostream>
#include "Programa.h"
#ifndef GRAFO_H
#define GRAFO_H

class Grafo {
    private:
    public:
      // se intancia el constructor
      Grafo();
      // funciones
      void crear_grafo(Nodo *nodo);
      void recorrer(Nodo *p);
      void preorden(Nodo *p);
      void inorden(Nodo *p);
      void posorden(Nodo *p);

};
#endif

#include <fstream>
#include <iostream>
#include <list>
#include "Grafo.h"
using namespace std;
// se crea los nodos
Nodo *crearNodo(int dato) {
    Nodo *q;
    q = new Nodo();
    q->izq = NULL;
    q->der = NULL;
    q->info = dato;
    return q;
}
// se busca el maximo posible osea la ultima derecha de un nodo
Nodo *maximo(Nodo *&p){
  if(p == NULL){
    return NULL;
  }
  while(p->der != NULL){
    p = p->izq;
    maximo(p);
  }
  return p;
}

Nodo *eliminar_nodo(Nodo *&p, int numero_eleminar){
  Nodo *aux;
    // el nodo no tiene ningun hijo osea es un nodo hoja
    if(p->der == NULL && p->izq == NULL){
      // se elimina p y se deja apuntando a null
      delete p;
      p = NULL;
    // si solo tiene un hijo izquierdo
    }else if(p->der == NULL){
      // se hace que el hijo del que se desea elimiar apunte al nodo padre del que se desea eliminar y se elimina
      aux = p;
      p = aux->izq;
      delete aux;
      // se hace que el hijo del que se desea elimiar apunte al nodo padre del que se desea eliminar y se elimina
    }else if(p->izq == NULL){
      aux = p;
      p = aux->der;
      delete aux;
    // si tiene dos hijos se hace que estos apunten a los padres del que se desea elimiar y se borra
    }else{
      aux = maximo(p->der);
      p->info = aux->info;
      p->der = eliminar_nodo(p->der,aux->info);
    }
  return p;
}

void buscar_nodo(Nodo *&p, int numero_eleminar){
  // se busca el nodo mediente recursividad
  if(numero_eleminar < p->info){
    p->izq = eliminar_nodo(p->izq, numero_eleminar);
  }else if(numero_eleminar > p->info){
    p->der = eliminar_nodo(p->der, numero_eleminar);
  }else{
    // se le envia el nodo a la funcon elimiar
    p = eliminar_nodo(p, numero_eleminar);
  }
}

int repetidos(list<int> numeros_arbol, int dato){
  bool existe;
  // se busca el valor en una lista que contiene todos los numeros agregados en el arbol
  for(int i:numeros_arbol){
    if(i == dato){
      existe = true;
    }
  }
  // si esta repetido le pide un nuevo numero al usuario, sale cuando agrega uno que no esta repetido
  if(existe == true){
    cout << "El nodo ya existe" << endl;
    cout << "inserte el numero a agregar numero: ";
    cin >> dato;
    repetidos(numeros_arbol, dato);
  }
  // retorna el dato
  return dato;
}
void modificar(Nodo *&q, int numero_modificar, list<int> &numeros_arbol){
  int nuevo;
  // se busca el nodo por recursividad
  if(numero_modificar < q->info){
    modificar(q->izq, numero_modificar, numeros_arbol);
  }if(numero_modificar > q->info){
    modificar(q->der, numero_modificar, numeros_arbol);
  }if(numero_modificar == q->info){
    // se le agrega la nueva informacion al nodo
    cout << "inserte el nuevo numero: ";
    cin >> nuevo;
    // se busca si esta repetido y se agrega el nuemro a la lista
    nuevo = repetidos(numeros_arbol, numero_modificar);
    numeros_arbol.push_back(nuevo);
    q->info = nuevo;
  }else{
    // sino el nodo que se desea modificar no existe
    cout << "El nodo que desea modificar no existe" << endl;
  }
}

void insertar(Nodo *&p, int nodo, int nuevo, list<int> &numeros_arbol){
  // se busca el nodo por recursividad
  if(nodo < p->info){
    insertar(p->izq, nodo, nuevo, numeros_arbol);
  }else if(nodo > p->info){
    insertar(p->der, nodo, nuevo, numeros_arbol);
  }
  if(nodo == p->info){
    // si es menor y el lado izquierdo esta vacio se agrega al lado izquierdo
    if(nuevo < p->info && p->izq == NULL){
      // se busca si esta repetido y se agrega el nuemro a la lista
      nuevo = repetidos(numeros_arbol, nuevo);
      numeros_arbol.push_back(nuevo);
      p->izq = crearNodo(nuevo);
      // si es mayor y el lado derecho esta vacio se agrega al lado derecho
    }else if(nuevo > p->info && p->der == NULL){
      // se busca si esta repetido y se agrega el nuemro a la lista
      nuevo = repetidos(numeros_arbol, nuevo);
      numeros_arbol.push_back(nuevo);
      p->der = crearNodo(nuevo);
    }
  }else{
    // sino no se agrega
    cout << "no se puedo agregar el nodo" << endl;
  }
}

void crear_arbol(Nodo *&raiz, Grafo g, list<int> &numeros_arbol){
  bool boolean = true;
  int informacion;
  int respuesta;
  // se agrea el primer nodo
  if(raiz == NULL){
    cout << "inserte el primer numero a agregar: ";
    cin >> informacion;
    raiz = crearNodo(informacion);
    numeros_arbol.push_back(informacion);
    crear_arbol(raiz, g, numeros_arbol);
  }else{
    // pregunta si desea agregar a la izquierda del nodo
    cout << "Desea crear un nodo por la izquierda a " << raiz->info <<"; si(1) - no(0): ";
    cin >> respuesta;
    if(respuesta == 1){
      // le pide el numero que desea agregar
      cout << "inserte el numero a agregar numero: ";
      cin >> informacion;
      // como a la izquierda van los menores le pide un numero menor
      while (informacion > raiz->info){
        cout << "A la izquierda van los numeros menores" << endl;
        cout << "inserte el numero a agregar numero: ";
        cin >> informacion;
      }
      // se busca si esta repetido y se agrega el nuemro a la lista
      informacion = repetidos(numeros_arbol, informacion);
      numeros_arbol.push_back(informacion);
      // se agrega el numero a la izquierda y se aplica recursividad para el siguiente nodo
      raiz->izq = crearNodo(informacion);
      crear_arbol(raiz->izq, g, numeros_arbol);
    }else{
      // sino desea agregar un numero a la izquierda apunta a NULL
        raiz->izq = NULL;
    }
    // le pregunta si desea agregar un numero a la derecha
    cout << "Desea crear un nodo por la derecha a " << raiz->info << "; si(1) - no(0): ";
    cin >> respuesta;
    if(respuesta == 1){
      // pide el numero a ingresar
    cout << "inserte el numero a agregar numero: ";
    cin >> informacion;
    // compueba que sea mayor
      while (informacion < raiz->info){
        cout << "A la derecha van los numeros mayores" << endl;
        cout << "inserte el numero a agregar numero: ";
        cin >> informacion;
      }
      // se busca si esta repetido y se agrega el nuemro a la lista
      informacion = repetidos(numeros_arbol, informacion);
      numeros_arbol.push_back(informacion);
      // se agrega al lado derecho y se usa recursividad para el siguiente nodo
      raiz->der = crearNodo(informacion);
      crear_arbol(raiz->der, g, numeros_arbol);
    }else{
      // sino apunta
      raiz->der = NULL;
    }
  }
}

int main(void) {
  int condicion = 0;
  // se cra la lista que contendra todos los numeros del arbol
  list<int> numeros_arbol;
  int opcion;
  Nodo *raiz = NULL;
  // se instancia la clase
  Grafo g = Grafo();


  do {
    cout << "MENU" << endl;
    cout << "1.- Crear arbol" << endl;
    cout << "2.- Agregar numero" << endl;
    cout << "3.- Eliminar numero" << endl;
    cout << "4.- Modificar elemento" << endl;
    cout << "5.- Mostrar el arbol en: preorden, inorden, posorden" << endl;
    cout << "6.- Salir" << endl;

    cout << "Ingrese una opcion: ";
    cin >> opcion;

    switch(opcion){
      case 1:
      system("clear");
      // crear el arbol para poder usar las demas opciones
      if(condicion == 0){
        crear_arbol(raiz, g, numeros_arbol);
        g.crear_grafo(raiz);
        condicion ++;
      }else{
        // una vez creado no se puede acceder a esta opcion
        cout << "El arbol ya fue creado" << endl;
      }
      break;

      case 2:
      system("clear");
      if(condicion == 0){
        cout << "Debe crear el arbol primero" << endl;
      }else{
        // una vez creado el arbol
        int nodo;
        int nuevo;
        // se pregunta el nuevo numero y el nodo
        cout << "inserte el nodo en el que desea agregar: ";
        cin >> nodo;
        cout << "inserte el nuevo numero: ";
        cin >> nuevo;
        insertar(raiz, nodo, nuevo, numeros_arbol);
        g.crear_grafo(raiz);

      }
      break;

      case 3:
      system("clear");
      if(condicion == 0){
        cout << "Debe crear el arbol primero" << endl;
      }else{
        system("clear");
        // se pide el numero que desea eliminar
        int numero_eleminar;
        cout << "inserte el numero a eliminar: ";
        cin >> numero_eleminar;
        buscar_nodo(raiz, numero_eleminar);
        g.crear_grafo(raiz);
      }
      break;

      case 4:
      system("clear");
      if(condicion == 0){
        cout << "Debe crear el arbol primero" << endl;
      }else{
        system("clear");
        // se pide el nodo que desea eliminar
        int numero_modificar;
        cout << "inserte el numero a modificar: ";
        cin >> numero_modificar;
        modificar(raiz, numero_modificar, numeros_arbol);
        g.crear_grafo(raiz);
      }
      break;

      case 5:
      system("clear");
      if(condicion == 0){
        cout << "Debe crear el arbol primero" << endl;
      }else{
        // se imprime los numeros en distintas formas de ordenamiento
        system("clear");
        cout << "----Preorden----" << endl;
        g.preorden(raiz);
        cout << endl;

        cout << "----Inorden----" << endl;
        g.inorden(raiz);
        cout << endl;

        cout << "----Posorden----" << endl;
        g.posorden(raiz);
        cout << endl;
      }
      break;
    }
  }while (opcion != 6);

    return 0;
}

// se define la el struct nodo y las funciones
typedef struct _Nodo {
    int info;
    struct _Nodo *izq;
    struct _Nodo *der;
    _Nodo *crearNodo();
    _Nodo *eliminar_nodo();
    _Nodo *maximo();
    void *modificar();
    void buscar_nodo();
    void crear_arbol();
    void insertar();
    int repetidos();


} Nodo;
